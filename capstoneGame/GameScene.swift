//
//  GameScene.swift
//  MultiPlayer Game
//
//  Created by Satinder pal Singh on 2019-12-02.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    var bomb : SKSpriteNode?
    let background1 = SKSpriteNode(imageNamed: "bg")
    let background2 = SKSpriteNode(imageNamed: "bg")
    let cat = SKSpriteNode(imageNamed: "Run1")
    let dog = SKSpriteNode(imageNamed: "dogRun1")
    var stopBackground = false
    var counterTimer = Timer()
       var counter = 0
       var boxArray:[SKSpriteNode] = [SKSpriteNode]()
    
    //adding the stones
    let stone1 = SKSpriteNode(imageNamed: "stone")
       let stone2 = SKSpriteNode(imageNamed: "stone")
       let stone3 = SKSpriteNode(imageNamed: "stone")
       let stone4 = SKSpriteNode(imageNamed: "stone")
       let stone5 = SKSpriteNode(imageNamed: "stone")
    
    var numStone1 = 10
    var numStone2 = 10
    var numStone3 = 10
    var numStone4 = 10
    var numStone5 = 10
    
    var stone1Label:SKLabelNode!
    var stone2Label:SKLabelNode!
    var stone3Label:SKLabelNode!
    var stone4Label:SKLabelNode!
    var stone5Label:SKLabelNode!
    
    
    func definingphysics(){
        let catTexture = SKTexture(imageNamed: "Run1.png")
        self.cat.physicsBody = SKPhysicsBody(texture: catTexture, size: catTexture.size())
        self.cat.name = "cat"
        self.cat.physicsBody?.affectedByGravity = false
        self.cat.physicsBody?.categoryBitMask = 2
        self.cat.physicsBody?.collisionBitMask = 0
        self.cat.physicsBody?.contactTestBitMask = 9
        self.cat.physicsBody?.allowsRotation = false
        
        
        
    }
    func backgroundPlacement(){
        background1.position = CGPoint(x: frame.size.width / 2, y:frame.size.height / 2)
        background1.size = CGSize(width: frame.width, height: frame.height)
        background1.anchorPoint = CGPoint.zero
        background1.position = CGPoint(x: 0, y: 70)
        background1.zPosition = -15
        self.addChild(background1)

        background2.size = CGSize(width: frame.width, height: frame.height)
        background2.anchorPoint = CGPoint.zero
        background2.position = CGPoint(x: background1.size.width - 1,y: 70)
        background2.zPosition = -15
        self.addChild(background2)
    }
    
    func moveBackgroundLoop(){
        background1.position = CGPoint(x: background1.position.x-4, y: background1.position.y)
        background2.position = CGPoint(x: background2.position.x-4, y: background2.position.y)
        
        for (index, box) in self.boxArray.enumerated() {
                   box.position.x = box.position.x-4
                   
               }
        if(background1.position.x < -background1.size.width)
        {
            background1.position = CGPoint(x: background1.size.width-9,y: 70)
        }
        if(background2.position.x < -background2.size.width)
        {
            background2.position = CGPoint(x: background1.size.width-9,y: 70)
        }
    }

    
    func spawnBomb(){
        

        self.bomb = self.childNode(withName: "bomb") as? SKSpriteNode
        
        // Throw the orange
        let throwOrangeAction = SKAction.applyImpulse(
            CGVector(dx: 200, dy: 400),
            duration: 0.25)
        self.bomb?.run(throwOrangeAction)
        
        let throwOrangeAction1 = SKAction.applyImpulse(
            CGVector(dx: 30, dy: 20),
            duration: 1)
        self.bomb?.run(throwOrangeAction1)
        
        
    }
    
    override func didMove(to view: SKView) {
        
        // Get label node from scene and store it for use later
        self.physicsWorld.contactDelegate = self
        
        definingphysics()
        backgroundPlacement()
        cat.position = CGPoint(x:self.size.width*0.25, y:240)
        addChild(cat)
        catAnimation()
        dog.position = CGPoint(x:self.size.width*0.22, y:290)
        addChild(dog)
        dogAnimation()
        startCounter()

        spawnBomb()
        makeStone()
        
    }
    func didBegin(_ contact: SKPhysicsContact)
    {
        
        print("Something collided!")
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        if (nodeA == nil || nodeB == nil)
        {
            return
        }
        
        
        if (nodeA!.name == "cat" && nodeB!.name == "bomb" || nodeA!.name == "bomb" && nodeB!.name == "cat")
        {
            print("A: \(nodeA!.name)  b: \(nodeB!.name)")
            catDeadAnimation()
            bombBlastAnimation()
            stopBackground = true
            
        }
        else if(nodeA!.name == "cat" && nodeB!.name == "box" || nodeA!.name == "box" && nodeB!.name == "cat")
        {
            catDeadAnimation()
            stopBackground = true
            print("A: \(nodeA!.name)  b: \(nodeB!.name)")
        }
    }
    
    func makeStone(){
        stone1.position = CGPoint(x: 10, y:80)
        stone1.size = CGSize(width: 267, height: 60)
        stone1.anchorPoint = CGPoint.zero
        stone1.zPosition = -13
        self.addChild(stone1)
        
        
        stone2.position = CGPoint(x: 270, y:80)
        stone2.size = CGSize(width: 267, height: 60)
        stone2.anchorPoint = CGPoint.zero
        stone2.zPosition = -13
        self.addChild(stone2)
        
        stone3.position = CGPoint(x: 537, y:80)
        stone3.size = CGSize(width: 267, height: 60)
        stone3.anchorPoint = CGPoint.zero
        stone3.zPosition = -13
        self.addChild(stone3)
        
        stone4.position = CGPoint(x: 804, y:80)
        stone4.size = CGSize(width: 267, height: 60)
        stone4.anchorPoint = CGPoint.zero
        stone4.zPosition = -13
        self.addChild(stone4)
        
        stone5.position = CGPoint(x: 1071, y:80)
        stone5.size = CGSize(width: 267, height: 60)
        stone5.anchorPoint = CGPoint.zero
        stone5.zPosition = -13
        self.addChild(stone5)
        
        
        // adding number label on stones
        self.stone1Label = SKLabelNode(text: "\(self.numStone1)")
               self.stone1Label.position = CGPoint(x:120, y:90)
               self.stone1Label.fontColor = UIColor.magenta
               self.stone1Label.fontSize = 55
               self.stone1Label.fontName = "Avenir"
               addChild(self.stone1Label)
        
        self.stone2Label = SKLabelNode(text: "\(self.numStone2)")
        self.stone2Label.position = CGPoint(x:380, y:90)
        self.stone2Label.fontColor = UIColor.magenta
        self.stone2Label.fontSize = 55
        self.stone2Label.fontName = "Avenir"
        addChild(self.stone2Label)
        
        self.stone3Label = SKLabelNode(text: "\(self.numStone3)")
        self.stone3Label.position = CGPoint(x:647, y:90)
        self.stone3Label.fontColor = UIColor.magenta
        self.stone3Label.fontSize = 55
        self.stone3Label.fontName = "Avenir"
        addChild(self.stone3Label)
        
        self.stone4Label = SKLabelNode(text: "\(self.numStone4)")
        self.stone4Label.position = CGPoint(x:914, y:90)
        self.stone4Label.fontColor = UIColor.magenta
        self.stone4Label.fontSize = 55
        self.stone4Label.fontName = "Avenir"
        addChild(self.stone4Label)
        
        self.stone5Label = SKLabelNode(text: "\(self.numStone5)")
        self.stone5Label.position = CGPoint(x:1181, y:90)
        self.stone5Label.fontColor = UIColor.magenta
        self.stone5Label.fontSize = 55
        self.stone5Label.fontName = "Avenir"
        addChild(self.stone5Label)
        
    }
    func boxfirst(yValue: CGFloat) {
        let boxTexture = SKTexture(imageNamed: "box.png")
        let box = SKSpriteNode(imageNamed: "box")
        
        box.physicsBody = SKPhysicsBody(texture: boxTexture, size: boxTexture.size())
        box.name = "box"
        box.physicsBody?.affectedByGravity = false
        box.physicsBody?.categoryBitMask = 8
        box.physicsBody?.collisionBitMask = 0
        box.physicsBody?.contactTestBitMask = 0
        box.physicsBody?.allowsRotation = true
         box.position = CGPoint(x: size.width+50, y:yValue+70)
         box.size = CGSize(width: 160, height: 160)
         //box.anchorPoint = CGPoint.zero
        box.zPosition = -13
         self.addChild(box)
        self.boxArray.append(box)
                 print(boxArray.count)
        
    }
    func startCounter(){
               counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
           }
           @objc func incrementCounter(){
            self.counter = self.counter + 1
            spawnBomb()
            if(self.counter % 3 == 0 && self.stopBackground == false){
                
            let randomInt = Int.random(in: 1...3)
            print("random number \(randomInt)")
                      if(randomInt == 1){
                        boxfirst(yValue: 160)
                      }
                      if(randomInt == 2){
                        boxfirst(yValue: 160)
                         boxfirst(yValue: 235)
                      }
                      if(randomInt == 3){
                        boxfirst(yValue: 160)
                        boxfirst(yValue: 235)
                        boxfirst(yValue: 310)
                      }
            }
            
        }
    func catAnimation(){
        if(self.stopBackground == false)
        {
            let image1 = SKTexture(imageNamed: "Run1")
            let image2 = SKTexture(imageNamed: "Run2")
            let image3 = SKTexture(imageNamed: "Run3")
            let image4 = SKTexture(imageNamed: "Run4")
            let image5 = SKTexture(imageNamed: "Run5")
            let image6 = SKTexture(imageNamed: "Run6")
            let image7 = SKTexture(imageNamed: "Run7")
            let image8 = SKTexture(imageNamed: "Run8")
            let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
            let punchAnimation = SKAction.animate(
                           with: punchTextures,
                           timePerFrame: 0.1)
            self.cat.run(punchAnimation)
            let catAnimationForeever = SKAction.repeatForever(punchAnimation)
            self.cat.run(catAnimationForeever)
            cat.zPosition = 1
        }
    }
    func dogAnimation(){
         let image1 = SKTexture(imageNamed: "dogRun1")
         let image2 = SKTexture(imageNamed: "dogRun2")
         let image3 = SKTexture(imageNamed: "dogRun3")
         let image4 = SKTexture(imageNamed: "dogRun4")
         let image5 = SKTexture(imageNamed: "dogRun5")
         let image6 = SKTexture(imageNamed: "dogRun6")
         let image7 = SKTexture(imageNamed: "dogRun7")
         let image8 = SKTexture(imageNamed: "dogRun8")
         let punchTextures = [image1,image2,image3,image4,image5,image6,image7,image8]
         let punchAnimation = SKAction.animate(
                        with: punchTextures,
                        timePerFrame: 0.1)
         self.dog.run(punchAnimation)
         let dogAnimationForeever = SKAction.repeatForever(punchAnimation)
         self.dog.run(dogAnimationForeever)
        
    }
    func catJumpAnimation(){

        let image1 = SKTexture(imageNamed: "Jump1")
        let image2 = SKTexture(imageNamed: "Jump4")
     //   let image3 = SKTexture(imageNamed: "Jump3")
        let image4 = SKTexture(imageNamed: "Jump2")
        let punchTextures = [image1,image2,image4]
        let punchAnimation = SKAction.animate(
                       with: punchTextures,
                       timePerFrame: 0.6)
        self.cat.run(punchAnimation)
        cat.zPosition = 1
        
    }
    func catDeadAnimation(){

        let image1 = SKTexture(imageNamed: "catdead1")
        let image2 = SKTexture(imageNamed: "catdead2")
        let image3 = SKTexture(imageNamed: "catdead3")
        let image4 = SKTexture(imageNamed: "catdead4")
        let punchTextures = [image1,image2,image3,image4]
        let punchAnimation = SKAction.animate(
                       with: punchTextures,
                       timePerFrame: 1)
        self.cat.run(punchAnimation)
        cat.zPosition = 1
        
    }
    func bombBlastAnimation(){
          let image1 = SKTexture(imageNamed: "pow")
                     let punchTextures = [image1]
                     let punchAnimation = SKAction.animate(
                                    with: punchTextures,
                                    timePerFrame: 0.5)
//        rocket.runAction(explode, completion: {
//          rocket.removeFromParent()
//        })
        self.bomb?.run(punchAnimation, completion: {
            self.bomb?.removeFromParent()
        })
          
      }
    func touchDown(atPoint pos : CGPoint) {
//        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
//            n.position = pos
//            n.strokeColor = SKColor.green
//            self.addChild(n)
//        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
//        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
//            n.position = pos
//            n.strokeColor = SKColor.blue
//            self.addChild(n)
//        }
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    var counterJump = 0;
    func jumpingcatdog()  {
        print("jump")
        catJumpAnimation()
        let jumpUpAction = SKAction.moveBy(x: 0, y:150, duration:0.9)
        let jumpDownAction = SKAction.moveBy(x: 0, y:-150, duration:0.9 )
        let jumpSequence = SKAction.sequence([jumpUpAction, jumpDownAction])
        cat.run(jumpSequence)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       // let location = (touches.first as! UITouch).location(in: self.view)
        // stone tapped
              guard let mousePosition = touches.first?.location(in: self) else {
                    return
                }
                if self.stone1.contains(mousePosition){
                    self.numStone1 = self.numStone1 - 1
                    self.stone1Label.text = "\(self.numStone1)"
                    spawnStone(stone: self.stone1)
                }
                
                else if self.stone2.contains(mousePosition){
                    self.numStone2 = self.numStone2 - 1
                    self.stone2Label.text = "\(self.numStone2)"
                    spawnStone(stone: self.stone2)
                }
                
                else if self.stone3.contains(mousePosition){
                    self.numStone3 = self.numStone3 - 1
                    self.stone3Label.text = "\(self.numStone3)"
                    spawnStone(stone: self.stone3)
                }
                
                else if self.stone4.contains(mousePosition){
                    self.numStone4 = self.numStone4 - 1
                    self.stone4Label.text = "\(self.numStone4)"
                    spawnStone(stone: self.stone4)
                }
                
                else if self.stone5.contains(mousePosition){
                    self.numStone5 = self.numStone5 - 1
                    self.stone5Label.text = "\(self.numStone5)"
                    spawnStone(stone: self.stone5)
                }
        
            else if (mousePosition.y > (self.size.height)/2){
                jumpingcatdog()
            }
            else{
            if mousePosition.x < (self.size.width)/2 {
                print("left")
                stopBackground = true
                if(self.cat.position.x >= 100)
                {
                    self.cat.position.x -= 15;
                    print("\(self.cat.position.x)");

                }
               // MoveLeft()
            } else {
                // right code
                stopBackground = false
                   print("right")
                if(self.cat.position.x <= (self.size.width)/1.5)
                {
                    self.cat.position.x += 15;
                    print("\(self.cat.position.x)");
                }

            }
        }
        
      
    }
    func spawnStone(stone : SKSpriteNode) {
           
        
           let stoneX = SKSpriteNode(imageNamed: "stone")
        stoneX.anchorPoint = CGPoint.zero
        stoneX.position.x = stone.position.x
        stoneX.position.y = stone.position.y
        stoneX.size = CGSize(width: 267, height: 60)
        // 3. Force orange to always appear in foreground
        stoneX.zPosition = 999

        // 4. show the orange on screen
       
        
        addChild(stoneX)
           
        let move1 = SKAction.move(to: CGPoint(x: stone.position.x , y: size.height),
                                          duration: 2)
          
                let stoneAnimation = SKAction.sequence(
                    [move1]
                )
              
                stoneX.run(stoneAnimation)
           
           
       }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if(stopBackground == false)
        {
            moveBackgroundLoop()
        }
    }
    
}
